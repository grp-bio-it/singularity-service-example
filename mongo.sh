#!/bin/bash

#SBATCH --mem 8G
#SBATCH -t 10:00
#SBATCH --gres=tmp:1G

# Make sure the directories exists
MONGOPATH=/scratch/sing-training/moscardo/singularity-service-example

echo "Copying DDBB"
time cp -ra $MONGOPATH/data $TMPDIR

# --verbose flag can be added to Singularity for debugging purposes.
echo "Running database"
time singularity instance start -B $MONGOPATH/log:/var/log/mongodb -B $MONGOPATH:$MONGOPATH -B $TMPDIR/data:/var/lib/mongo -B $MONGOPATH/mongoconf:/etc/mongo/ $MONGOPATH/mongo_4.0.6.img  mongo

# Giving some time to start up the DDBB, useful when not shutted down properly 
sleep 30

# 
#Do your computation here
# 

# Insert results data into the DDBB
time singularity  exec instance://mongo mongo 127.0.0.1:26016/testddbb --eval 'var document = [{name  : "John",position : "Teacher",}, {name  : "Marie",position : "Doctor",}];db.MyCollection.insert(document);'

# Getting results from a query
echo "Query ...."
time singularity  exec instance://mongo  mongoexport -d testddbb --port 26016 -c MyCollection --query '{"position":{"$eq": "Teacher" }}' --out $TMPDIR/test.out --type csv --fields name,position

sleep 5
echo "Copying in memmory results to FS"
time cp $TMPDIR/test.out $MONGOPATH

sleep 5
echo "Stopping DDBB instance"
time singularity instance stop mongo

echo "Copying DDBB to FS"
time cp -ra $TMPDIR/data $MONGOPATH/data-mod

